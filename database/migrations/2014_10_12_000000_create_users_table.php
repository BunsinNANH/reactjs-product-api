<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
        DB::table("users")->insert([
            'name'  =>  "Admin",
            'email'  =>  "admin@gmail.com",
            'password'  =>  bcrypt("admin@123"),
            "created_at"    => Carbon\Carbon::now("Asia/Phnom_Penh"),
            "updated_at"    => Carbon\Carbon::now("Asia/Phnom_Penh"),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
